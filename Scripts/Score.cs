﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour 
{
	float totalTimeElapsed = 0;
	float score = 0f;
	bool isGameOver = false;

	public GUISkin skin;
	public ButtonScript starter;

	void Start () 
	{
		Time.timeScale = 1;
	}

	void Update () 
	{
		if (isGameOver)
			return;
		if (starter.isStarted == true) 
		{
			totalTimeElapsed += Time.deltaTime;
			score = totalTimeElapsed * 100;

			if (Time.timeScale == 0)
			{
				isGameOver = true;
			}
		}
	}

	void OnGUI()
	{
		GUI.skin = skin;

		if (starter.isStarted == true) 
		{
			if (!isGameOver) 
			{
				GUI.Label (new Rect (Screen.width - (Screen.width / 6), 10, Screen.width / 5, Screen.height / 5), "SCORE: " + ((int)score).ToString ());
			} 

			else
			{
				Time.timeScale = 0;
				GUI.Box (new Rect (50, 50, Screen.width -100, Screen.height -100), "GAME OVER\n YOUR SCORE: " + (int)score);

				if (GUI.Button (new Rect (Screen.width / 3, Screen.height / 2 + 20, Screen.width / 3, Screen.height / 12), "Restart")) 
				{
					Application.LoadLevel (Application.loadedLevel);
				}
			}
		}
	}
}