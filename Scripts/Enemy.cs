﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	public float speed = 0.75f;

	void Update ()
	{
		transform.Translate(0,0,speed*Time.deltaTime);
	}
}
