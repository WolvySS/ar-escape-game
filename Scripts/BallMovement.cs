﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class BallMovement : MonoBehaviour 
{
	float dirX;
	public float speed = 100f;
	private Rigidbody rb;

	void Start ()
	{
		rb = GetComponent<Rigidbody> ();
	}

	void Update ()
	{
		dirX = CrossPlatformInputManager.GetAxis ("Horizontal");
	}
		
	void FixedUpdate () 
	{
		rb.velocity = new Vector3 (dirX * speed, 0, 0);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Enemy") 
		{
			Time.timeScale = 0;
		}

	}
}
