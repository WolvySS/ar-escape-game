﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour 
{
	public GameObject enemy;
	public bool start = false;
	float timeElapsed = 0;
	float spawnCycle = 0.7f;

	public ButtonScript starter;

	void Start () 
	{
		this.gameObject.SetActive (false);
	}

	void Update () 
	{
		if (starter.isStarted==true)
		{
		timeElapsed += Time.deltaTime;

			if (timeElapsed > spawnCycle) 
			{
				GameObject temp;

				temp = (GameObject)Instantiate (enemy);
				Vector3 pos = temp.transform.position;
				temp.transform.position = new Vector3 (Random.Range (-0.7f, 0.7f), pos.y, pos.z);
				timeElapsed -= spawnCycle;
			}
		}
	}

}
