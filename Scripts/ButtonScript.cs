﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ButtonScript : MonoBehaviour, IVirtualButtonEventHandler
{
	public GameObject startButton;
	public GameObject GameController;

	public GameObject vButton;

	public bool isStarted = false;

	void Start()
	{
		startButton = GameObject.Find ("StartButton");
		startButton.GetComponent<VirtualButtonBehaviour> ().RegisterEventHandler (this);
	}

	void Update()
	{
		
	}

	public void OnButtonPressed(VirtualButtonAbstractBehaviour vb)
	{
		isStarted = true;
		//Debug.Log ("Game Started");
		GameController.SetActive (true);
	}

	public void OnButtonReleased(VirtualButtonAbstractBehaviour vb)
	{
		//Debug.Log ("Button Removed");
		Destroy (vButton);
	}

}
